/*
 *    Copyright 2020 Django Cass
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 *
 */

import dev.dcas.gradle.boot
import dev.dcas.gradle.cloud
import org.springframework.boot.gradle.tasks.bundling.BootJar

plugins {
	id("org.springframework.boot") version "2.3.0.RELEASE"
	id("io.spring.dependency-management") version "1.0.9.RELEASE"
	id("dev.dcas.gradle-util") version "0.1"
	id("io.freefair.lombok") version "5.1.0"
	java
	maven
}

group = "dev.dcas"
version = "0.1"

val moduleName: String by extra("dev.dcas.alerts")
val javaHome: String = System.getProperty("java.home")

repositories {
	maven(url = "https://mvn.v2.dcas.dev")
	mavenCentral()
}

extra["springCloudVersion"] = "Hoxton.SR4"

dependencyManagement {
	imports {
		mavenBom("org.springframework.cloud:spring-cloud-dependencies:${property("springCloudVersion")}")
	}
}

dependencies {
	implementation(boot("starter-json"))
	implementation(cloud("starter-openfeign"))
}

configure<JavaPluginConvention> {
	sourceCompatibility = JavaVersion.VERSION_11
	targetCompatibility = JavaVersion.VERSION_11
}

tasks {
	// enable preview features
	withType<JavaCompile>().all {
		inputs.property("moduleName", moduleName)
		doFirst {
			options.compilerArgs = listOf(
				"--module-path", classpath.asPath,
				"--patch-module", "$moduleName=${sourceSets["main"].output.asPath}"
			)
			classpath = files()
		}
	}
	getByName<BootJar>("bootJar") {
		enabled = false
	}

	getByName<Jar>("jar") {
		enabled = true
	}
	withType<Wrapper> {
		gradleVersion = "6.3"
		distributionType = Wrapper.DistributionType.BIN
	}
	withType<Test> {
		useJUnitPlatform()
	}
}