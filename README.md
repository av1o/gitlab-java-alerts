# GitLab Alerts

This project contains a basic OpenFeign client for interacting with GitLab's generic alerts integration.

## Setup

Follow the [GitLab guide](https://gitlab.com/help/user/project/integrations/generic_alerts.md) for enabling Alerts integration.

**Add to your project**

```kotlin
implementation("com.github.av1o:gitlab-alerts:0.1")
```

```xml
<dependency>
    <groupId>com.github.av1o</groupId>
    <artifactId>gitlab-alerts</artifactId>
    <version>0.1</version>
</dependency>
```

**Set the GitLab url**

application.properties
```properties
gitlab-alerts.ribbon.listOfServers=https://gitlab.example.com
```

application.yaml
```yaml
gitlab-alerts:
  ribbon:
    listOfServers: https://gitlab.example.com
```

**Create an alert**

```java
class Example {
    @Autowired
    private AlertClient client;

    void doSomething() {
        // create the payload
        var payload = new Payload(
            "error",
            "something went wrong doing this thing",
            (Instant).now(),
            "my-service",
            "my-monitoring-tool",
            (List).of("192.168.1.234"),
            Severity.medium
        );
        // send the alert
        client.createAlert(payload, "Bearer " + token, "my-group/my-project");
    }   
}
```