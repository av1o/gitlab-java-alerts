/*
 *    Copyright 2020 Django Cass
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 *
 */

package dev.dcas.alerts.client;

import dev.dcas.alerts.model.Payload;
import feign.Response;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;

@FeignClient(value = "gitlab-alerts")
public interface AlertClient {

	/**
	 * Create an alert for a specified GitLab project
	 * https://gitlab.com/help/user/project/integrations/generic_alerts.md
	 * @param payload object containing default parameters
	 * @param token token found when enabling alerts (must be prepended with 'Bearer ')
	 * @param project the namespace of the target project (e.g. "foo/bar")
	 */
	@PostMapping("/{project}/alerts/notify.json")
	Response createAlert(
		@RequestBody Payload payload,
		@RequestHeader("Authorization") String token,
		@PathVariable String project
	);
}
