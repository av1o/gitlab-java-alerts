module dev.dcas.alerts {
	requires spring.cloud.openfeign.core;
	requires spring.web;
	requires feign.core;
	requires com.fasterxml.jackson.annotation;
	requires lombok;

	exports dev.dcas.alerts;
}